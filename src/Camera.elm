module Camera exposing (Camera(..), generateRay)

import Point exposing (Point)
import Ray exposing (Ray)
import Vector exposing (Vector)


-- MODEL


type Camera
    = PinholeCamera
        { lower_left_corner : Vector
        , horizontal : Vector
        , vertical : Vector
        , origin : Point
        }


generateRay : Camera -> Float -> Float -> Ray
generateRay camera u v =
    case camera of
        PinholeCamera c ->
            Ray.from c.origin (Vector.add c.lower_left_corner (Vector.add (Vector.scale u c.horizontal) (Vector.scale v c.vertical)))
