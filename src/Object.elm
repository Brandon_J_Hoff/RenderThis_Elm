module Object exposing (Object(..), hitObjects)

import Color exposing (Color)
import HitRecord exposing (HitRecord(..))
import Point exposing (Point)
import Ray exposing (Ray)
import Vector exposing (Vector)


type alias Center =
    Point


type alias Radius =
    Float


type Object
    = Sphere Center Radius


hitObjects : List Object -> Ray -> HitRecord -> ( Bool, HitRecord )
hitObjects objects ray hr =
    List.foldl (findClosestHit ray) ( False, hr ) objects


findClosestHit : Ray -> Object -> ( Bool, HitRecord ) -> ( Bool, HitRecord )
findClosestHit ray object ( current_hit, current_hr ) =
    let
        ( hit, hr ) =
            hitObject ray current_hr object
    in
    case hit of
        True ->
            ( True, hr )

        False ->
            ( current_hit, current_hr )


hitObject : Ray -> HitRecord -> Object -> ( Bool, HitRecord )
hitObject ray hr object =
    case object of
        Sphere center radius ->
            hitSphere center radius ray hr


hitSphere : Center -> Radius -> Ray -> HitRecord -> ( Bool, HitRecord )
hitSphere center radius ray (HitRecord hr) =
    let
        ray_direction =
            Ray.getDirection ray

        oc =
            Point.sub (Ray.getOrigin ray) center

        a =
            Vector.dot ray_direction ray_direction

        b =
            Vector.dot (Point.toVector oc) ray_direction

        c =
            Point.dot oc oc - (radius * radius)

        discriminant =
            b * b - a * c
    in
    if discriminant > 0 then
        let
            t1 =
                (-b - sqrt discriminant) / a
        in
        if t1 < hr.tmax && t1 > hr.tmin then
            let
                p1 =
                    Ray.pointAlongRay ray t1

                n1 =
                    Point.scale (1.0 / radius) (Point.sub p1 center) |> Point.toVector
            in
            ( True, HitRecord.from hr.tmin t1 t1 p1 n1 )
        else
            let
                t2 =
                    (-b + sqrt discriminant) / a
            in
            if t2 < hr.tmax && t2 > hr.tmin then
                let
                    p2 =
                        Ray.pointAlongRay ray t2

                    n2 =
                        Point.scale (1.0 / radius) (Point.sub p2 center) |> Point.toVector
                in
                ( True, HitRecord.from hr.tmin t2 t2 p2 n2 )
            else
                ( False, HitRecord hr )
    else
        ( False, HitRecord hr )
