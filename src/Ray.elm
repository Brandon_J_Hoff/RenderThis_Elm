module Ray exposing (Ray, from, getDirection, getOrigin, getUnitDirection, pointAlongRay, zero)

import Point exposing (Point)
import Vector exposing (Vector)


type Ray
    = Ray Point Vector Vector


zero : Ray
zero =
    Ray Point.origin Vector.zero Vector.zero


from : Point -> Vector -> Ray
from origin direction =
    Ray origin direction (Vector.normalize direction)


pointAlongRay : Ray -> Float -> Point
pointAlongRay (Ray origin direction _) t =
    Point.addVector origin (Vector.scale t direction)


getOrigin : Ray -> Point
getOrigin (Ray origin _ _) =
    origin


getDirection : Ray -> Vector
getDirection (Ray _ direction _) =
    direction


getUnitDirection : Ray -> Vector
getUnitDirection (Ray _ _ unit_direction) =
    unit_direction
