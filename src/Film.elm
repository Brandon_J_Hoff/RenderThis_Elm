module Film
    exposing
        ( Film
        , convertColor
        , from
        , getHeight
        , getSize
        , getWidth
        , isPixelActive
        , processImage
        , reset
        , updateFilm
        )

import Array exposing (Array)
import Color exposing (Color)
import Maybe


type Pixel
    = Pixel Bool Int Color Color


type Film
    = Film Int Int (Array Pixel)


from : Int -> Int -> Film
from width height =
    Film width height (Array.repeat (width * height) emptyPixel)


getWidth : Film -> Int
getWidth (Film width _ _) =
    width


getHeight : Film -> Int
getHeight (Film _ height _) =
    height


getSize : Film -> ( Int, Int )
getSize (Film width height _) =
    ( width, height )


reset : Film -> Film
reset (Film width height pixels) =
    Film width height (resetPixels pixels)


emptyPixel : Pixel
emptyPixel =
    Pixel True 0 Color.empty Color.empty


resetPixels : Array Pixel -> Array Pixel
resetPixels pixels =
    Array.map (\_ -> emptyPixel) pixels


updateFilm : Int -> Color -> Film -> Film
updateFilm index color (Film width height pixels) =
    let
        updated_pixel =
            Array.get index pixels
                |> Maybe.withDefault emptyPixel
                |> updatePixel color
    in
    Array.set index updated_pixel pixels
        |> Film width height


updatePixel : Color -> Pixel -> Pixel
updatePixel new_color (Pixel _ num_samples variant old_color) =
    let
        new_ns =
            num_samples + 1

        ( avg_color, sd ) =
            Color.standardDeviation old_color new_color variant new_ns

        active =
            List.any (\x -> x > 0.1) (Color.toList sd)
    in
    Pixel active new_ns sd avg_color


processImage : Film -> List Int
processImage (Film width height pixels) =
    List.concatMap convertColor (Array.toList pixels)


convertColor : Pixel -> List Int
convertColor (Pixel _ _ _ color) =
    Color.scale 255 color
        |> Color.clampColor
        |> Color.toListWithAlpha 255
        |> List.map floor


isPixelActive : Int -> Film -> Bool
isPixelActive index (Film _ _ pixels) =
    Array.get index pixels
        |> Maybe.withDefault emptyPixel
        |> (\(Pixel active _ _ _) -> active)
