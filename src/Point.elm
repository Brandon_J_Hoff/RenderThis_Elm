module Point exposing (Point(..), add, addVector, dot, from, origin, scale, sub, subVector, toVector)

import Vector exposing (Vector)


type Point
    = Point Vector


origin : Point
origin =
    Point Vector.zero


from : Float -> Float -> Float -> Point
from x y z =
    Point (Vector.from x y z)


add : Point -> Point -> Point
add (Point p1) (Point p2) =
    Point (Vector.add p1 p2)


addVector : Point -> Vector -> Point
addVector (Point p) v =
    Point (Vector.add p v)


sub : Point -> Point -> Point
sub (Point p1) (Point p2) =
    Point (Vector.sub p1 p2)


subVector : Point -> Vector -> Point
subVector (Point p) v =
    Point (Vector.sub p v)


scale : Float -> Point -> Point
scale x (Point p) =
    Point (Vector.scale x p)


dot : Point -> Point -> Float
dot (Point p1) (Point p2) =
    Vector.dot p1 p2


toVector : Point -> Vector
toVector (Point p) =
    p
