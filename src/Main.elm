-- https://ellie-app.com/37SJBtC2gJ5a1


port module Main exposing (DisplayModel, Model, Msg(..), Status(..), calculateColor, cameraSample, display, generateSamples, imageIndex, init, main, pinholeCamera, randomSample, removePixelIndex, renderNextPixel, renderPixel, scene, screenHeight, screenWidth, subscriptions, update, view)

import Array
import Browser
import Camera exposing (Camera)
import Color exposing (Color)
import Film exposing (Film)
import HitRecord exposing (HitRecord(..))
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import List
import Object exposing (Object(..), hitObjects)
import Point exposing (Point)
import Random exposing (Seed)
import Ray exposing (Ray)
import RenderThis exposing (Scene)
import Task
import Time exposing (Posix)
import Vector exposing (Vector)


port display : DisplayModel -> Cmd msg


port renderNextPixel : (() -> msg) -> Sub msg


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { scene : Scene
    , status : Status
    , pixelPool : List Int
    , seed : Int
    }


type alias DisplayModel =
    { width : Int
    , height : Int
    , pixels : List Int
    }


screenWidth : Int
screenWidth =
    100


screenHeight : Int
screenHeight =
    50


pinholeCamera : Camera
pinholeCamera =
    Camera.PinholeCamera
        { lower_left_corner = Vector.from -2.0 -1.0 -1.0
        , horizontal = Vector.from 4.0 0.0 0.0
        , vertical = Vector.from 0.0 2.0 0.0
        , origin = Point.origin
        }


scene : Scene
scene =
    Scene
        [ Sphere (Point.from 0.0 0.0 -1.0) 0.5
        , Sphere (Point.from 0.0 -100.5 -1.0) 100
        ]
        pinholeCamera
        (Film.from screenWidth screenHeight)


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model scene Stopped (List.range 0 (screenWidth * screenHeight - 1)) 0, Task.perform Update_Seed Time.now )



-- UPDATE


type Msg
    = Start_Render
    | Stop_Render
    | RenderPixel ( Int, ( Float, Float ) )
    | UpdatePixel Int Color
    | Display
    | CreateSample
    | Update_Seed Posix


type Status
    = Rendering
    | Stopped


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Start_Render ->
            ( { model | status = Rendering }, Random.generate RenderPixel (cameraSample model) )

        CreateSample ->
            ( model, Random.generate RenderPixel (cameraSample model) )

        RenderPixel ( index, sample ) ->
            case ( model.pixelPool, model.status ) of
                ( [], _ ) ->
                    update Stop_Render model

                ( _, Rendering ) ->
                    ( model, renderPixel model index sample )

                ( _, Stopped ) ->
                    ( model, Cmd.none )

        UpdatePixel index color ->
            let
                new_model =
                    { model | scene = RenderThis.updateFilm index color model.scene }

                is_active =
                    Film.isPixelActive index new_model.scene.film
            in
            case is_active of
                True ->
                    update CreateSample new_model

                False ->
                    update CreateSample { new_model | pixelPool = removePixelIndex index model.pixelPool }

        Display ->
            ( model
            , display { width = screenWidth, height = screenHeight, pixels = Film.processImage model.scene.film }
            )

        Stop_Render ->
            update Display { model | status = Stopped }

        Update_Seed posix ->
            ( { model | seed = Time.posixToMillis posix }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    renderNextPixel (always CreateSample)



-- Time.every 1000 Tick
-- VIEW


view model =
    case model.status of
        Rendering ->
            div []
                [ button [ onClick Stop_Render ] [ text "Stop" ]
                , div [] [ text <| (++) " Number of pixels left: " <| String.fromInt <| List.length model.pixelPool ]
                ]

        Stopped ->
            div []
                [ button [ onClick Start_Render ] [ text "Render" ]
                ]



-- FUNCTIONS


removePixelIndex : Int -> List Int -> List Int
removePixelIndex index pixels =
    List.filter (\i -> i /= index) pixels



-- renderPixles model =
--     List.map renderPixel (List.range 0 (screenWidth * screenHeight - 1))


renderPixel : Model -> Int -> ( Float, Float ) -> Cmd Msg
renderPixel model index ( s1, s2 ) =
    let
        ( width, height ) =
            Film.getSize model.scene.film

        x =
            modBy width index

        y =
            height - (index // width)

        u =
            (toFloat x + s1) / toFloat width

        v =
            (toFloat y + s2) / toFloat height

        ray =
            Camera.generateRay model.scene.camera u v
    in
    calculateColor index ray model.scene s1
        |> Task.succeed
        |> Task.perform (UpdatePixel index)


calculateColor : Int -> Ray -> Scene -> Float -> Color
calculateColor index ray s random =
    let
        ( hit, HitRecord hr ) =
            hitObjects s.objects ray HitRecord.empty

        normal =
            Vector.toRecord hr.normal
    in
    if hit then
        let
            seed =
                Random.initialSeed <| floor (100.0 * random)

            target =
                Point.add (Point.addVector hr.point hr.normal) <| randomPointOnSphere seed

            ( new_random, new_seed ) =
                Random.step (Random.float 0 1) seed
        in
        calculateColor index (Ray.from hr.point (Point.toVector (Point.sub target hr.point))) s new_random
            |> Color.scale 0.5

    else
        let
            unit_direction =
                Ray.getUnitDirection ray |> Vector.toRecord

            t2 =
                0.5 * (unit_direction.y + 1.0)
        in
        Color.add (Color.scale (1.0 - t2) (Color.from 1.0 1.0 1.0)) (Color.scale t2 (Color.from 0.5 0.7 1.0))


randomPointOnSphere : Seed -> Point
randomPointOnSphere seed =
    let
        ( x, seed1 ) =
            Random.step (Random.float 0 1) seed

        ( y, seed2 ) =
            Random.step (Random.float 0 1) seed1

        ( z, seed3 ) =
            Random.step (Random.float 0 1) seed2

        p =
            Point.sub (Point.scale 2.0 (Point.from x y z)) (Point.from 1 1 1)
    in
    if Point.dot p p >= 1.0 then
        randomPointOnSphere seed3

    else
        p


cameraSample : Model -> Random.Generator ( Int, ( Float, Float ) )
cameraSample model =
    Random.pair (imageIndex model) randomSample


imageIndex : Model -> Random.Generator Int
imageIndex model =
    case model.pixelPool of
        [] ->
            Random.constant 0

        x :: xs ->
            Random.uniform x xs


randomSample : Random.Generator ( Float, Float )
randomSample =
    Random.pair (Random.float 0 1) (Random.float 0 1)


generateSamples : Int -> Random.Seed -> ( List ( Float, Float ), Random.Seed )
generateSamples num seed0 =
    Random.step (Random.list num randomSample) seed0
