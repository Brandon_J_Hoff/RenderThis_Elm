module RenderThis exposing (Scene, updateFilm)

import Array
import Camera exposing (Camera)
import Color exposing (Color)
import Film exposing (Film)
import Math.Vector3 as Vector3 exposing (Vec3, vec3)
import Object exposing (Object(..), hitObjects)
import Point exposing (Point)


-- MODEL


type alias Scene =
    { objects : List Object
    , camera : Camera
    , film : Film
    }



-- UPDATE


type Msg
    = RENDER
    | STOP


updateFilm : Int -> Color -> Scene -> Scene
updateFilm index color scene =
    { scene | film = Film.updateFilm index color scene.film }
