module HitRecord exposing (HitRecord(..), empty, from)

import Point exposing (Point(..))
import Vector exposing (Vector(..))


type HitRecord
    = HitRecord
        { tmin : Float
        , tmax : Float
        , t : Float
        , point : Point
        , normal : Vector
        }


empty : HitRecord
empty =
    HitRecord
        { tmin = 0.0
        , tmax = 1000000.0
        , t = 0.0
        , point = Point.origin
        , normal = Vector.zero
        }


from : Float -> Float -> Float -> Point -> Vector -> HitRecord
from tmin tmax t point normal =
    HitRecord { tmin = tmin, tmax = tmax, t = t, point = point, normal = normal }
