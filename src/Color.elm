module Color
    exposing
        ( Color
        , add
        , addScaler
        , clampColor
        , empty
        , from
        , fromRecord
        , length
        , mul
        , scale
        , standardDeviation
        , sub
        , toList
        , toListWithAlpha
        , toRecord
        )


type Color
    = Color Float Float Float


empty : Color
empty =
    Color 0 0 0


from : Float -> Float -> Float -> Color
from r g b =
    Color r g b


fromRecord : { r : Float, g : Float, b : Float } -> Color
fromRecord record =
    Color record.r record.g record.b


toRecord : Color -> { r : Float, g : Float, b : Float }
toRecord (Color r g b) =
    { r = r, g = g, b = b }


toList : Color -> List Float
toList (Color r g b) =
    [ r, g, b ]


toListWithAlpha : Float -> Color -> List Float
toListWithAlpha a (Color r g b) =
    [ r, g, b, a ]


clampColor : Color -> Color
clampColor (Color r g b) =
    Color (clamp 0 255 r) (clamp 0 255 g) (clamp 0 255 b)


add : Color -> Color -> Color
add (Color r1 g1 b1) (Color r2 g2 b2) =
    Color (r1 + r2) (g1 + g2) (b1 + b2)


addScaler : Float -> Color -> Color
addScaler x (Color r g b) =
    Color (r + x) (g + x) (b + x)


sub : Color -> Color -> Color
sub (Color r1 g1 b1) (Color r2 g2 b2) =
    Color (r1 - r2) (g1 - g2) (b1 - b2)


mul : Color -> Color -> Color
mul (Color r1 g1 b1) (Color r2 g2 b2) =
    Color (r1 * r2) (g1 * g2) (b1 * b2)


scale : Float -> Color -> Color
scale x (Color r g b) =
    Color (r * x) (g * x) (b * x)


length : Color -> Float
length (Color r g b) =
    sqrt ((r * r) + (g * g) + (b * b))


average : Color -> Color -> Int -> Color
average c1 c2 n =
    if n <= 1 then
        c2
    else
        sub c2 c1
            |> scale (1.0 / toFloat n)
            |> add c1


standardDeviation : Color -> Color -> Color -> Int -> ( Color, Color )
standardDeviation old_color new_color variant n =
    if n <= 1 then
        ( new_color, empty )
    else
        let
            mean =
                average old_color new_color n

            deviation =
                mul (sub new_color old_color) (sub new_color mean)
                    |> add variant
                    |> scale (1.0 / toFloat (n - 1))
                    |> map sqrt
        in
        ( mean, deviation )


map : (Float -> Float) -> Color -> Color
map fn (Color r g b) =
    Color (fn r) (fn g) (fn b)
