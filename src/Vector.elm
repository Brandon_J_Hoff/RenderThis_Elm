module Vector exposing (Vector, add, dot, from, fromRecord, length, normalize, scale, sub, toRecord, zero)


type Vector
    = Vector Float Float Float


zero : Vector
zero =
    Vector 0 0 0


from : Float -> Float -> Float -> Vector
from x y z =
    Vector x y z


fromRecord : { x : Float, y : Float, z : Float } -> Vector
fromRecord r =
    Vector r.x r.y r.z


toRecord : Vector -> { x : Float, y : Float, z : Float }
toRecord (Vector x y z) =
    { x = x, y = y, z = z }


add : Vector -> Vector -> Vector
add (Vector x1 y1 z1) (Vector x2 y2 z2) =
    Vector (x1 + x2) (y1 + y2) (z1 + z2)


sub : Vector -> Vector -> Vector
sub (Vector x1 y1 z1) (Vector x2 y2 z2) =
    Vector (x1 - x2) (y1 - y2) (z1 - z2)


scale : Float -> Vector -> Vector
scale s (Vector x y z) =
    Vector (x * s) (y * s) (z * s)


dot : Vector -> Vector -> Float
dot (Vector x1 y1 z1) (Vector x2 y2 z2) =
    (x1 * x2) + (y1 * y2) + (z1 * z2)


length : Vector -> Float
length v =
    sqrt (dot v v)


normalize : Vector -> Vector
normalize v =
    scale (1 / length v) v
